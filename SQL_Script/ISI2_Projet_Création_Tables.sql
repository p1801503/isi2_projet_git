create table Admin
(
	Id_A int not null
		primary key,
	Username varchar(50) null,
	Mdp varchar(50) null
);

create table Catégorie
(
	Id_C int not null
		primary key,
	Nom varchar(50) null
);

create table Contrat
(
	Id_D int not null
		primary key,
	Prix int null,
	Surface int null,
	DateContrat date null,
	Id_D_1 int not null,
	constraint Contrat_ibfk_1
		foreign key (Id_D_1) references Contrat (Id_D)
);

create index Id_D_1
	on Contrat (Id_D_1);

create table Inscription
(
	Id_I int not null
		primary key,
	DateInscription date null
);

create table InscriptionCertifié
(
	Id_I int not null
		primary key,
	RIB int null,
	DateCertifiation date null,
	Id_A int not null,
	constraint InscriptionCertifié_ibfk_1
		foreign key (Id_I) references Inscription (Id_I),
	constraint InscriptionCertifié_ibfk_2
		foreign key (Id_A) references Admin (Id_A)
);

create index Id_A
	on InscriptionCertifié (Id_A);

create table Sous_Catégorie
(
	Id_C_1 int not null,
	Id_SC int not null,
	NomSC varchar(50) null,
	Id_C int not null,
	Id_SC_1 int not null,
	primary key (Id_C_1, Id_SC),
	constraint Sous_Catégorie_ibfk_1
		foreign key (Id_C_1) references Catégorie (Id_C),
	constraint Sous_Catégorie_ibfk_2
		foreign key (Id_C, Id_SC_1) references Sous_Catégorie (Id_C_1, Id_SC)
);

create table Service
(
	Id_C int not null,
	Id_SC int not null,
	Id_S int not null,
	NomSer varchar(50) null,
	primary key (Id_C, Id_SC, Id_S),
	constraint Service_ibfk_1
		foreign key (Id_C, Id_SC) references Sous_Catégorie (Id_C_1, Id_SC)
);

create table ServiceEtudiant
(
	Id_SE varchar(50) not null
		primary key,
	Tarrif int null,
	Disponibilites varchar(50) null,
	Id_C int not null,
	Id_SC int not null,
	Id_S int not null,
	constraint ServiceEtudiant_ibfk_1
		foreign key (Id_C, Id_SC, Id_S) references Service (Id_C, Id_SC, Id_S)
);

create table Offre
(
	Id_I int not null,
	Id_SE varchar(50) not null,
	primary key (Id_I, Id_SE),
	constraint Offre_ibfk_1
		foreign key (Id_I) references InscriptionCertifié (Id_I),
	constraint Offre_ibfk_2
		foreign key (Id_SE) references ServiceEtudiant (Id_SE)
);

create index Id_SE
	on Offre (Id_SE);

create index Id_C
	on ServiceEtudiant (Id_C, Id_SC, Id_S);

create index Id_C
	on Sous_Catégorie (Id_C, Id_SC_1);

create table Utilisateur
(
	Id_U int not null
		primary key,
	Prenom varchar(50) null,
	Nom varchar(50) null,
	Mail varchar(50) null,
	Mdp varchar(50) null,
	Username varchar(50) null,
	Id_I int null,
	Id_I_1 int not null,
	constraint Utilisateur_ibfk_1
		foreign key (Id_I) references Inscription (Id_I),
	constraint Utilisateur_ibfk_2
		foreign key (Id_I_1) references Inscription (Id_I)
);

create table Client_e_
(
	Id_U int not null
		primary key,
	Adresse varchar(50) null,
	constraint Client_e__ibfk_1
		foreign key (Id_U) references Utilisateur (Id_U)
);

create table ChoixService
(
	Id int not null,
	Id_D int not null,
	Id_SE varchar(50) not null,
	primary key (Id, Id_D, Id_SE),
	constraint ChoixService_ibfk_1
		foreign key (Id) references Client_e_ (Id_U),
	constraint ChoixService_ibfk_2
		foreign key (Id_D) references Contrat (Id_D),
	constraint ChoixService_ibfk_3
		foreign key (Id_SE) references ServiceEtudiant (Id_SE)
);

create index Id_D
	on ChoixService (Id_D);

create index Id_SE
	on ChoixService (Id_SE);

create table Etudiant_e_
(
	Id_U int not null
		primary key,
	description varchar(50) null,
	Ville varchar(50) null,
	constraint Etudiant_e__ibfk_1
		foreign key (Id_U) references Utilisateur (Id_U)
);

create index Id_I
	on Utilisateur (Id_I);

create index Id_I_1
	on Utilisateur (Id_I_1);

create table categories
(
	id bigint unsigned auto_increment
		primary key,
	created_at timestamp null,
	updated_at timestamp null
)
collate=utf8mb4_unicode_ci;

create table failed_jobs
(
	id bigint unsigned auto_increment
		primary key,
	uuid varchar(255) not null,
	connection text not null,
	queue text not null,
	payload longtext not null,
	exception longtext not null,
	failed_at timestamp default current_timestamp() not null,
	constraint failed_jobs_uuid_unique
		unique (uuid)
)
collate=utf8mb4_unicode_ci;

create table migrations
(
	id int auto_increment
		primary key,
	migration varchar(255) null,
	batch int null
);

create table password_resets
(
	email varchar(255) not null,
	token varchar(255) not null,
	created_at timestamp null
)
collate=utf8mb4_unicode_ci;

create index password_resets_email_index
	on password_resets (email);

create table services
(
	id bigint unsigned auto_increment
		primary key,
	created_at timestamp null,
	updated_at timestamp null
)
collate=utf8mb4_unicode_ci;

create table users
(
	id bigint unsigned auto_increment
		primary key,
	name varchar(255) not null,
	email varchar(255) not null,
	email_verified_at timestamp null,
	password varchar(255) not null,
	remember_token varchar(100) null,
	created_at timestamp null,
	updated_at timestamp null,
	constraint users_email_unique
		unique (email)
)
collate=utf8mb4_unicode_ci;

