<?php

namespace App\Http\Controllers;

use App\Models\Etudiant;
use Illuminate\Http\Request;

class EtudiantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$etudiants = Etudiant::all();
        //return view('etudiant', compact($etudiants));
        $etudiant = new Etudiant();
        $etudiants = $etudiant->getAll();
        return view('etudiant', compact('etudiants'));
    }

    public function show(int $id)
    {
        $etudiant = new Etudiant();
        $etudiant->getOne($id);
        return view('etudiantProfil', compact('etudiant'));
    }
}
