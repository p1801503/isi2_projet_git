<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    //provide data to the home page
    public function index(){
        return view('home');
    }
}
