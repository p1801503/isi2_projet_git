<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Sous_Categorie;

class SousCategorieController extends Controller
{

    public function index(){
        $sous_categorie = new Sous_Categorie();
        $sous_categories = $sous_categorie ->getAllSous_Cat();
        return view('sous_categorie', compact('sous_categories'));
    }

    public function show(Sous_Categorie $services){
        //$service = new Sous_Categorie();
        //$services = $service ->getService($id_c, $id_sc);
        $services ->getService((int)$services->Id_C, (int)$services->Id_SC);
        return view('service', compact('services'));
    }
}
