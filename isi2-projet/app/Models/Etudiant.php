<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class Etudiant extends Model
{
    use HasFactory;
    
    // obtenir la liste des utilisateurs
    public function getAll(){
        //$etudiants = DB::table('Utilisateur')->get();
        $etudiants = DB::select('SELECT * FROM Utilisateur U JOIN Etudiant_e_ E ON U.Id_U = E.Id_U');
        return $etudiants;
    }

    public function getOne(int $id){
        $etudiant = DB::table('Utilisateur')
        ->join('Etudiant_e_', 'Etudiant_e_.Id_U', '=', 'Utilisateur.Id_U')
        ->where('Etudiant_e_.Id_U', '=', $id)->get();
        return $etudiant;
    }

    /*
    protected $table = 'Etudiant_e_';
    protected $fillable = ['Id_U', 'description', 'Ville'];
    */

}
