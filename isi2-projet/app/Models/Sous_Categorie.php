<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class Sous_Categorie extends Model
{
    use HasFactory;

    // obtenir la liste des utilisateurs
    public function getAllSous_Cat(){
        $sous_categories = DB::table('Sous_Catégorie')
        ->join('Catégorie','Catégorie.Id_C', '=', 'Sous_Catégorie.Id_C')->get();
        return $sous_categories;
    }

    public function getService(int $id_c, int $id_sc){
        $services = DB::table('Service')
        ->join('Sous_Catégorie','Sous_Catégorie.Id_SC', '=', 'Service.Id_SC')
        ->where('Service.Id_SC','=', $id_sc)
        ->where('Service.Id_C',"=", $id_c)->get();
        return $services;
    }    
    
}
