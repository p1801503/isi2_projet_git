Pour installer le projet : 

1. faire un git clone dans son dossier /websites (là où l'on place les fichier des sites à tester)
2. copier les fichiers du projet dans un autre répertoire et laissé les fichiers git
3. ouvrir Visual Studio Code avec la commande <b><i>`code .`</i></b> dans le terminal Linux
4. creer le projet Laravel avec la commande : <b><i>`composer create-project Laravel/Laravel nom_projet`</i></b>
5. donner l'accès aux fichiers avec les commande : 
        <ul>`sudo chown -R $maxime :www-data storage`</ul>
        <ul>`sudo chown -R $maxime :www-data bootstrap/cache`</ul>
        <ul>`sudo chmod -R 775 storage`</ul>
        <ul>`sudo chmod -R 775 bootstrap/cache`</ul>
6. modifier le fichier 'composer.json' en ajoutant la ligne <b>"`illuminate/filesystem": "^8.42`"</b> à la ligne 14
7. installer breeze : 
        <ul>Dans le terminal terminal Linux (au niveau de la racine du projet Laravel) entrer la commande : <b><i>`composer require laravel/breeze --dev`</i></b></ul>
        <ul>toujours dans le terminal Linux : entrer la commande <b><i>`sudo /usr/bin/php artisan breeze:install`</i></b></ul>
        <ul>revenir dans le terminal de VS Code et entrer la commande <b><i>`npm install && npm run dev`</i></b></ul>
    </br></br>
8. replacé les fichiers du projet originel dans le dossier du projet Laravel créer, accepté les remplacements 
9. dans datagrip on créer la base de données 'ISI2_Projet' et on exécute le script sql du git pour la remplir
10. ajouter un utilisateur de la base de données en lui accordant l'accès en exécutant le script : 
        <ul><b><i>`create user NomUser@localhost identified by 'MotDePass';`</b></i></ul>
	    <ul><b><i>`grant all privileges on ISI2_Projet.* to NomUser@localhost;`</b></i></ul>
	    <ul><b><i>`flush privileges;`</b></i></ul>
11. enfin modifier les lignes 10 à 15 du fichier .env dans le projet Laravel : 
>     DB_CONNECTION=mysql
>     DB_HOST=127.0.0.1
>     DB_PORT=3306
>     DB_DATABASE=ISI2_Projet
>     DB_USERNAME=NomUser
>     DB_PASSWORD=MotDePass

Vous pouvez maintenant entrez dans votre navigateur l'url : localhost/[chemin du dossier]/ 
Vous arriverez sur la page d'acceuil sur laquelle vous pouvez vous inscrire / connecté ou consulté les catégories etc...
