@extends('layouts.layout')
@section('titrePage')
Alo
@endsection


@section('titreItem')
<h2>Alo, le site qu'il vous faut</h2>
<img class=imgHome src='https://projetisi2.files.wordpress.com/2021/06/pexels-photo-2850438.jpeg'/>
@endsection

@section('contenu')

<h2>Un site qui vous permettra de trouver de l'aide rapidement</h2>
<div>
    <li><strong>Trouvez</strong> l'offre qui vous correspond à vos besoins parmi les annonces proposées par les étudiants</li>
    <li><strong>Réservez</strong> votre service après la mise en relation avec l'étudiant</li>
    <li><strong>Payez</strong> via une plateforme 100% sécurisé</li>
</div>
@endsection