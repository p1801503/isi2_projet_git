@extends('layouts.layout')

@section('titrePage')
    Etudiants
@endsection

@section('titreItem')
    <h2>Voici la liste de nos étudiants</h2>
@endsection

@section('contenu')
<table class ="table table-borderd table-stripted">
    <thead>
        <th>Prénom</th>
        <th>Nom</th>
        <th>E-mail</th>
        <th>Description</th>
        <th>Pour voir le profil</th>
    </thead>

    @foreach($etudiants as $etudiant)
        <tr>
            <td> {{ $etudiant->Prenom}} </td>
            <td> {{ $etudiant->Nom}} </td>
            <td> {{ $etudiant->Mail}} </td>
            <td> {{ $etudiant->description}} </td>
            <td><a class="btn btn-primary" href="{{route('etudiant.show', $etudiant->Id_U)}}"> 
                    <button class="btn btn-primary">Voir</button>
                </a>
            </td>
        </tr>
    @endforeach
</table>
@endsection
