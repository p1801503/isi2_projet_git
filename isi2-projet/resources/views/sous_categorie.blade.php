@extends('layouts.layout')

@section('titrePage')
    Info étudiant
@endsection

@section('titreItem')
    <h2>Liste de nos catégories</h2>
@endsection

@section('contenu')

    <div class="card">
        <header class="card-header">
            <h5 class="card-header-title">Extérieur</h5>
        </header>
        @foreach($sous_categories as $sous_categorie)
            @if($sous_categorie->Id_C == 1)
            
            <div class="list-group-item" style="text-align: center;">
                <a href="{{route('sous_categorie.show', $sous_categorie->Id_SC)}}">{{$sous_categorie->NomSC}}</a>
            </div>
    
            @endif
        @endforeach    
    </div>

    <div class="card" >
        <header class="card-header">
            <h5 class="card-header-title">Intérieur</h5>
        </header>
        @foreach($sous_categories as $sous_categorie)
            @if($sous_categorie->Id_C == 2)
            <div class="list-group-item" style="text-align: center;">
                <a href="{{route('sous_categorie.show', $sous_categorie->Id_SC)}}">{{$sous_categorie->NomSC}}</a>
            </div>
            @endif
        @endforeach    
    </div>

@endsection