@extends('layouts.layout')

@section('titrePage')
    Info étudiant
@endsection

@section('titreItem')
    <h2>Profil de l'étudiant</h2>
@endsection

@section('contenu')

<?php print_r($etudiant) ?>

<div class="card">
    <header class="card-header">
        <h5 class="card-header-title">Prenom : {{$etudiant->Prenom}}, Nom : {{$etudiant->Nom}}</h5>
    </header>
    <div class = "card-content">
        <div class="content">
            <p>Description : {{$etudiant->description}} Random</p>
            <p>Ville : {{$etudiant->ville}} Random</p>
            <hr>
            <p>Disponibilité :</p>
        </div>
    </div>
</div>

@endsection