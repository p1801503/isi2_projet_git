<!doctype html>
<html lang="fr">
	<head>
        <meta charset="utf-8">

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
                
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Baloo+Tammudu+2&display=swap" rel="stylesheet"> 
        
        <link href="{{ asset('css/style.css')}}" rel="stylesheet">
        
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>

        <title>
            @yield('titrePage')
        </title>
    </head>
     
    <body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <div class="container-fluid">
                <a class="navbar-brand" href="{{url('/') }}">
                    <img class="logo" src="img/ALO.png"/>
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">

                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="{{url('/') }}">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Actualités</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('etudiant.index')}}">Etudiants</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('sous_categorie.index')}}">Catégories</a>
                        </li>
                        <li class="nav-item dropdown">
                            @auth
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                Bienvenu, {{Auth::user()->name}}
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                <li>
                                    <a class="dropdown-item" href="{{route('logout')}}" method="POST" onclick="event.preventDefaut();
                                            document.getElementById('logout-form').submit();">
                                         {{__('Logout')}}
                                    </a>
                                    <form id="logout-form" action="{{route('logout')}}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </li>
                            </ul>
                            @else
                            <li>
                                <a class="nav-link active" aria-current="page" href="{{route('login')}}">Login</a>
                            </li>
                            <li>
                                <a class="nav-link active" aria-current="page" href="{{route('register')}}">Register</a>
                            </li>
                            @endauth
                        </li>
                    </ul>
                    
                </div>
            </div>
        </nav>
        
        
        <header>
            @yield('titreItem')
        </header>

        <section>
            @yield('contenu')
        </section>
    </body>

    <footer class="footer">
        Alo - copyright 3AInfo - 2021
    </footer>
</html>