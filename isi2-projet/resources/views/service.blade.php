@extends('layouts.layout')

@section('titrePage')
    Service
@endsection

@section('titreItem')
    <h2>Voici la liste de nos services pour la sous-catégorie</h2>
@endsection

@section('contenu')

    <div class="card" >
        <header class="card-header">
            <h5 class="card-header-title">Intérieur</h5>
        </header>
        @foreach($services as $service)
            <div class="list-group-item" style="text-align: center;">
                <li>random</li>
            </div>
        @endforeach    
    </div>

@endsection